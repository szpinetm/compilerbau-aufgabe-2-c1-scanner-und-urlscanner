%option noinput nounput noyywrap yylineno

%{
    #include "urlscanner.h"
    int fileno(FILE *stream);
%}

%s FIND_LINK
%s EXTRACT_LINK
%s EXTRACT_LINKTEXT

%%

\<a { BEGIN(FIND_LINK); }
<FIND_LINK>[ \t\n]+href[ \t]*=[ \t]*\" { BEGIN(EXTRACT_LINK); }
<FIND_LINK>\> { BEGIN(INITIAL); }

<EXTRACT_LINK>[^\"]* { 
    yylval = yytext;
    return TOKEN_URL;
}
<EXTRACT_LINK>\"[^\>]*\> { BEGIN(EXTRACT_LINKTEXT); }

<EXTRACT_LINKTEXT>[^\<]* {
    /*  fails when there are tags inside linktext (e.g. "<span...>")...
        then the text will eventually be broken up into more than one token.
    */
    yylval = yytext;
    return TOKEN_TEXT;
}
<EXTRACT_LINKTEXT>\<\/a[ \t\n]*\> { BEGIN(INITIAL); }

(.|\n)      {}
<<EOF>> {return EOF;}

%%
