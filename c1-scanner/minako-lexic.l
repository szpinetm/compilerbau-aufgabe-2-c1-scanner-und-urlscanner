%option noinput nounput noyywrap

DIGIT [0-9]
INTEGER {DIGIT}+
FLOAT {INTEGER}"."{INTEGER}|"."{INTEGER}
LETTER [a-zA-Z]
%x COMMENT

%{
    #include "minako.h"
    #include <stdlib.h>
    #include <stdio.h>
    int fileno(FILE *stream);
%}

%%

"/*"            { BEGIN(COMMENT); }
<COMMENT>"*/"   { BEGIN(INITIAL); }
<COMMENT>.|\n

"//".*      {/*return 3000;*/}

"bool"      {return KW_BOOLEAN;}
"do"        {return KW_DO;}
"else"      {return KW_ELSE;}
"float"     {return KW_FLOAT;}
"for"       {return KW_FOR;}
"if"        {return KW_IF;}
"int"       {return KW_INT;}
"printf"    {return KW_PRINTF;}
"return"    {return KW_RETURN;}
"void"      {return KW_VOID;}
"while"     {return KW_WHILE;}

"+" {return '+';}
"-" {return '-';}
"*" {return '*';}
"/" {return '/';}
"=" {return '=';}

"=="    {return EQ;}
"!="    {return NEQ;}
"<"     {return LSS;}
">"     {return GRT;}
"<="    {return LEQ;}
">="    {return GEQ;}
"&&"    {return AND;}
"||"    {return OR;}

"," {return ',';}
";" {return ';';}
[(] {return '(';}
[)] {return ')';}
[{] {return '{';}
[}] {return '}';}

{INTEGER}   {
    yylval.intValue = atoi(yytext); return CONST_INT;
}
{FLOAT}([eE]([+-])?{INTEGER})?|{INTEGER}[eE]([+-])?{INTEGER}    {
    yylval.floatValue = atof(yytext);
    return CONST_FLOAT;
}
"true"|"false"  {
    yylval.intValue = (strcmp(yytext, "true") == 0) ? 1 : 0;return CONST_BOOLEAN;
}
"\""[^\n\"]*"\""    {
    if (yyleng == 2) {
        yylval.string = NULL;
    } else {
        yylval.string = yytext;
        for (int j = 1; j <= yyleng-2; ++j)
            yylval.string[j-1] = yylval.string[j];
        yylval.string[yyleng-2] = '\0';
    }
    return CONST_STRING;
}
({LETTER})+({DIGIT}|{LETTER})*  {yylval.string = yytext; return ID;}

[ \t]+    {/*return 1000;*/}
[\n] {yylineno++; /*return 600;*/}

<<EOF>> {return EOF;}
. {fprintf(stderr, "ERROR: invalid character\n");}

%%
